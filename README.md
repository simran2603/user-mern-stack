
### To Setup

1. `cd backend`
2. `npm install`
3. `cd frontend`
4. `npm install`

### To Run
To run node server
1. `cd backend`
2. `node server.js`

To run react frontend
1. `cd frontend`
2. `npm start`
